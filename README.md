# coder-desafio-23

Normalizr


## endpoints:

[GET]/api/productos - Lista todos los productos

[GET]/api/productos/:id - Trae un único producto por id

[POST] /api/productos - Guarda un producto en la base

[PUT] /api/productos/:id - Modifica un producto por id

[DELETE] /api/productos/:id - Elimina un producto por id

## UI

/ingreso

### src/utils/normalizer.js

Clase con la implementación de normalizr y schemas del lado del back

### src/resources/input-chat.js

Llamado a denormalize desde el front y calculo de la compresión
